import React from 'react'
import { render } from 'react-dom'
// import './index.css'
import App from './client/layouts/App'
import registerServiceWorker from './lib/service/registerServiceWorker'

const args = [
    <App />,
    document.getElementById('root')
]

render(...args)
registerServiceWorker()
