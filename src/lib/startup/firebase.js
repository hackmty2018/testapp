import firebase from 'firebase'

const config = {
    apiKey          : 'AIzaSyB4oOpicpzjIeAHgobs8NTIONTZASx9Kxc',
    authDomain      : 'hackmty2018-kencho.firebaseapp.com',
    databaseURL     : 'https://hackmty2018-kencho.firebaseio.com/',
    storageBucket   : 'hackmty2018-kencho.appspot.com'
}

const app = firebase.initializeApp(config)
const googleProvider = new firebase.auth.GoogleAuthProvider()
const facebookProvider = new firebase.auth.FacebookAuthProvider()
const storageRef = firebase.storage().ref()
const database = firebase.database()

export { app, googleProvider, facebookProvider, storageRef, database }
