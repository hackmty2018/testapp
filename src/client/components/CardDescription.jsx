import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import CardContent from '@material-ui/core/CardContent'
import { withStyles } from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider'

const styles = () => ({
    container: {
        border: '2px solid black'
    }
})

const CardDescription = ({
    desc
}) => (
    <Fragment>
        <CardContent>
            <Typography component="p">
                { desc }
            </Typography>
        </CardContent>
        <Divider />
    </Fragment>
)

CardDescription.propTypes = {
    desc: PropTypes.string.isRequired
}

export default withStyles(styles)(CardDescription)
