import React, { Component, Fragment } from 'react'
import compose from 'recompose/compose'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import tileData from './titleData'

const styles = theme => ({
    root: {
        display         : 'flex',
        flexWrap        : 'wrap',
        justifyContent  : 'space-around',
        overflow        : 'auto'
    },
    gridList: {
        width                           : '60%',
        height                          : '100%',
        backgroundColor                 : '#F5F5F5',
        [theme.breakpoints.down('md')]  : {
            width: '80%'
        }
    },
    subheader: {
        width: '100%'
    },
    fab: {
        position        : 'absolute',
        overflow        : 'auto',
        top             : 100,
        right           : 100
    },
    imagen: {
        width: '100%'
    }
})

class GridImagenes extends Component {
    static propTypes = {
        classes: PropTypes.object.isRequired
    }

    state = {
        cols: 9
    }

    // _handlerCols = () => {
    //     const mq = window.matchMedia('(min-width: 960px)')
    //     if (mq.matches) {
    //         this.setState({ cols: 6 })
    //     }
    // }

    render () {
        const { cols } = this.state
        const { classes } = this.props
        return (
            <Fragment>
                <div className={ classes.root }>
                    <GridList cellHeight="auto" className={ classes.gridList } cols={ cols }>
                        {tileData.map(tile => (
                            <GridListTile key={ tile.img } cols={ tile.cols || 1 }>
                                <img src={ tile.img } alt={ tile.title } className={ classes.imagen } />
                            </GridListTile>
                        ))}
                    </GridList>
                </div>
            </Fragment>
        )
    }
}

GridImagenes.propTypes = {
    classes: PropTypes.object.isRequired
}

export default compose(
    withStyles(styles, { withTheme: true })
)(GridImagenes)
