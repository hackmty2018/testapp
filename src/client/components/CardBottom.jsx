import React, { Fragment } from 'react'
import IconButton from '@material-ui/core/IconButton'
import FavoriteIcon from '@material-ui/icons/Favorite'
import ShareIcon from '@material-ui/icons/Share'
import { withStyles } from '@material-ui/core'
import red from '@material-ui/core/colors/red'
import PropTypes from 'prop-types'

const styles = () => ({
    container: {
        backgroundColor: red[500]
    }
})

const CardBottom = ({
    classes
}) => (
    <Fragment>
        <IconButton aria-label="Like" className={ classes.container }>
            <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="Share">
            <ShareIcon />
        </IconButton>
    </Fragment>
)

CardBottom.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CardBottom)
