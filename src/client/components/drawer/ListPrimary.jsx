import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import StarIcon from '@material-ui/icons/Star'
import SendIcon from '@material-ui/icons/Send'

const styles = () => ({
    // menuItem: {
    //     '&:focus': {
    //         backgroundColor       : theme.palette.primary.main,
    //         '& $primary, & $icon' : {
    //             color: theme.palette.common.white
    //         }
    //     }
    // }
})

const Primary = () => (
    <MenuList>
        <MenuItem
            selected
        >
            <ListItemIcon>
                <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Popular" />
        </MenuItem>
        <MenuItem>
            <ListItemIcon>
                <StarIcon />
            </ListItemIcon>
            <ListItemText primary="Trending" />
        </MenuItem>
        <MenuItem>
            <ListItemIcon>
                <SendIcon />
            </ListItemIcon>
            <ListItemText primary="New" />
        </MenuItem>
    </MenuList>
)

export default withStyles(styles, { withTheme: true })(Primary)
