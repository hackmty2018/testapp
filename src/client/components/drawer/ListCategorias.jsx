import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Home from '@material-ui/icons/Home'
import Group from '@material-ui/icons/Group'
import { Link } from 'react-router-dom'

const styles = () => ({
    // menuItem: {
    //     '&:focus': {
    //         backgroundColor       : theme.palette.primary.main,
    //         '& $primary, & $icon' : {
    //             color: theme.palette.common.white
    //         }
    //     }
    // }
})

const Categorias = ({
    location
}) => (
    <MenuList>
        <MenuItem
            selected={ location.pathname === '/' }
            component={ Link }
            to="/"
        >
            <ListItemIcon>
                <Home />
            </ListItemIcon>
            <ListItemText primary="Home" />
        </MenuItem>
        <MenuItem
            selected={ location.pathname === '/comunidades' }
            component={ Link }
            to="/comunidades"
        >
            <ListItemIcon>
                <Group />
            </ListItemIcon>
            <ListItemText primary="Comunidades" />
        </MenuItem>
    </MenuList>
)

Categorias.propTypes = {
    location: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(Categorias)

