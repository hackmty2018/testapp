import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import Divider from '@material-ui/core/Divider'
// import ListPrimary from './ListPrimary'
import ListCategorias from './ListCategorias'
import ListComunidades from './ListComunidades'

const DrawerContent = ({
    location
}) => (
    <Fragment>
        {/* <Divider />
        <ListPrimary /> */}
        <Divider />
        <ListCategorias
            location={ location }
        />
        <Divider />
        <ListComunidades
            location={ location }
        />
    </Fragment>
)

DrawerContent.propTypes = {
    location: PropTypes.object.isRequired
}

export default DrawerContent
