import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import ListSubheader from '@material-ui/core/ListSubheader'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemText from '@material-ui/core/ListItemText'
// import MailIcon from '@material-ui/icons/Mail'
import Avatar from '@material-ui/core/Avatar'
import { Link } from 'react-router-dom'

const styles = () => ({
    // menuItem: {
    //     '&:focus': {
    //         backgroundColor       : theme.palette.primary.main,
    //         '& $primary, & $icon' : {
    //             color: theme.palette.common.white
    //         }
    //     }
    // }
})

const Categorias = ({
    location
}) => (
    <MenuList
        component="nav"
        subheader={ (
            <ListSubheader
                component="div"
            >
                Comunidades
            </ListSubheader>
        ) }
    >
        <MenuItem
            selected={ location.pathname === '/comunidades/poods' }
            component={ Link }
            to="/comunidades/poods"
        >
            <Avatar alt="Remy Sharp" src="https://yt3.ggpht.com/-rJq9gk1QIis/AAAAAAAAAAI/AAAAAAAAAAA/Kx4wkvKOfxY/s88-c-k-no-mo-rj-c0xffffff/photo.jpg" />
            <ListItemText primary="Home" />
        </MenuItem>
        <MenuItem
            selected={ location.pathname === '/comunidades/dolan' }
            component={ Link }
            to="/comunidades/dolan"
        >
            <Avatar alt="Remy Sharp" src="https://yt3.ggpht.com/-dPlY2nC8pL4/AAAAAAAAAAI/AAAAAAAAAAA/jB0ypPv2Wxo/s88-c-k-no-mo-rj-c0xffffff/photo.jpg" />
            <ListItemText primary="Comunidades" />
        </MenuItem>
    </MenuList>
)

Categorias.propTypes = {
    location: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(Categorias)

