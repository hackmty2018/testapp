import React from 'react'
import CardHeader from '@material-ui/core/CardHeader'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types'
import red from '@material-ui/core/colors/red'

const styles = theme => ({
    container: {
        [theme.breakpoints.down('xs')]: {
            maxWidth    : '30%',
            maxHeight   : '30%'
        }
    },
    avatar: {
        backgroundColor: red[500]
    },
    title: {
        backgroundColor: red[500]
    }
})

const CardTop = ({
    title,
    img
}) => (
    <CardHeader
        avatar={ (
            <Avatar alt="Remy Sharp" src={ img } />
        ) }
        action={ (
            <IconButton>
                <MoreVertIcon />
            </IconButton>
        ) }
        title={ title }
    />
)

CardTop.propTypes = {
    img     : PropTypes.string.isRequired,
    title   : PropTypes.string.isRequired
}

export default withStyles(styles)(CardTop)
