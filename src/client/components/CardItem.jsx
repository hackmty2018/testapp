import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import CardMedia from '@material-ui/core/CardMedia'
import Lightbox from 'react-images'

const styles = () => ({
    container: {
        height          : '100%',
        paddingTop      : '56.25%',
        backgroundSize  :     'contain'
    }
})

class CardItem extends Component {
    static propTypes = {
        classes     : PropTypes.object.isRequired,
        img         : PropTypes.string.isRequired
    }

    state = {
        openLightbox: false
    }

    _handleOpenLightbox = () => {
        this.setState({ openLightbox: true })
    }

    _handleCloseLightbox = () => {
        this.setState({ openLightbox: false })
    }

    render () {
        const {
            classes,
            img
        } = this.props
        const { openLightbox } = this.state
        return (
            <Fragment>
                <Lightbox
                    images={ [
                        { src: img }
                    ] }
                    isOpen={ openLightbox }
                    onClose={ this._handleCloseLightbox }
                />
                <CardMedia
                    onClick={ this._handleOpenLightbox }
                    className={ classes.container }
                    image={ img }
                    title="pogchamp"
                />
            </Fragment>
        )
    }
}

export default withStyles(styles)(CardItem)
