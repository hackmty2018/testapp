const tileData = [
    {
        img      : 'https://img.culturacolectiva.com/content/2018/07/03/1530671303183/memes-de-pena-nieto-y-amlo-buevitos-medium.png',
        title    : 'Breakfast',
        author   : 'jill111',
        cols     : 3,
        featured : true
    },
    {
        img    : 'https://i.pinimg.com/originals/72/ba/00/72ba0010c37c84c0b16a53bbfd5ae4eb.jpg',
        title  : 'Camera',
        cols   :  3,
        author : 'anson67'
    },
    {
        img    : 'https://scontent.fntr4-1.fna.fbcdn.net/v/t1.0-9/31949967_2073338796041444_6470626908714303488_n.jpg?_nc_cat=0&oh=6b00bfc55fdce7dc61069023aff86b20&oe=5BF26383',
        title  : 'Tasty burger',
        cols   :  3,
        author : 'director90'
    },
    {
        img      : 'https://em.wattpad.com/a74e4875b0bbcb30adc8810d06a2ba937aa78f8a/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f2d3566525964756a634175454b673d3d2d3534393435333236372e313531643362653338353233663834323232303338323430363630392e6a7067?s=fit&w=720&h=720',
        title    : 'Morning',
        author   : 'fancycrave1',
        cols     : 3,
        featured : true
    }
]

export default tileData
