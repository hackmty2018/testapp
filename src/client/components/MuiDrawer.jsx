import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Hidden from '@material-ui/core/Hidden'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import DrawerContent from './drawer/DrawerContent'

const drawerWidth = 240

const styles = theme => ({
    drawerPaper: {
        position : 'relative',
        width    : drawerWidth
    },
    drawerHeader: {
        display         : 'flex',
        alignItems      : 'center',
        justifyContent  : 'flex-end',
        padding         : '0 8px',
        ...theme.mixins.toolbar
    }
})

const MuiDrawer = ({
    open,
    classes,
    closeDrawer,
    location
}) => (
    <Fragment>
        <Hidden
            smDown
        >
            <Drawer
                variant="persistent"
                anchor="left"
                open={ open }
                classes={{
                    paper: classes.drawerPaper
                }}
            >
                <div className={ classes.drawerHeader }>
                    <IconButton onClick={ closeDrawer }>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <DrawerContent
                    location={ location }
                />
            </Drawer>
        </Hidden>
        <Hidden mdUp>
            <Drawer
                variant="temporary"
                anchor="left"
                open={ open }
                onClose={ closeDrawer }
                classes={{
                    paper: classes.drawerPaper
                }}
                ModalProps={{
                    keepMounted: true
                }}
            >
                <DrawerContent
                    location={ location }
                />
            </Drawer>
        </Hidden>

    </Fragment>
)

MuiDrawer.propTypes = {
    classes         : PropTypes.object.isRequired,
    closeDrawer     : PropTypes.func.isRequired,
    open            : PropTypes.bool,
    location        : PropTypes.object
}

MuiDrawer.defaultProps = {
    location    : {},
    open        : false
}

export default withStyles(styles, { withTheme: true })(MuiDrawer)
