import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import CardActions from '@material-ui/core/CardActions'

const styles = theme => ({
    card: {
        marginLeft                      : 'auto',
        marginRight                     : 'auto',
        display                         : 'flex',
        width                           : '60%',
        minHeight                       : 150,
        marginBottom                    : 20,
        maxHeight                       : 20,
        [theme.breakpoints.down('md')]   : {
            width: '100%'
        }
    },
    details: {
        flexDirection: 'column'
    },
    content: {
        flex            : '1 0 auto',
        textOverflow    : 'ellipsis',
        overflow        : 'hidden',
        maxHeight       : 60
    },
    cover: {
        marginLeft : 'auto',
        minWidth   : 151,
        maxWidth   : 151
    }
})

function KenchoList (props) {
    const { classes, title, descripcion, url } = props
    return (
        <Card className={ classes.card }>
            <div className={ classes.details }>
                <CardContent className={ classes.content }>
                    <Typography variant="headline">{ title }</Typography>
                    <Typography variant="subheading" color="textSecondary">
                        { descripcion }
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button variant="contained" color="secondary" className={ classes.button }>
                        Subscribirse
                    </Button>
                </CardActions>
            </div>
            <CardMedia
                className={ classes.cover }
                image={ url }
            />
        </Card>
    )
}

KenchoList.propTypes = {
    classes     : PropTypes.object.isRequired,
    title       : PropTypes.string.isRequired,
    descripcion : PropTypes.string.isRequired,
    url         : PropTypes.string.isRequired
}

export default withStyles(styles, { withTheme: true })(KenchoList)
