import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import red from '@material-ui/core/colors/red'
import CardBottom from './CardBottom'
import CardTop from './CardTop'
import CardDescription from './CardDescription'
import CardItem from './CardItem'

const styles = theme => ({
    card: {
        maxHeight                      : 800,
        marginLeft                     : '25%',
        marginRight                    : '25%',
        marginBottom                   : 25,
        [theme.breakpoints.down('xs')] : {
            maxWidth    : '100%',
            maxHeight   : '100%',
            marginLeft  : 0,
            marginRight : 0
        }
    },
    media: {
        height     : 100,
        paddingTop : '56.25%' // 16:9
    },
    actions: {
        display: 'flex'
    },
    expand: {
        transform  : 'rotate(0deg)',
        transition : theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest
        }),
        marginLeft                   : 'auto',
        [theme.breakpoints.up('sm')] : {
            marginRight: -8
        }
    },
    expandOpen: {
        transform: 'rotate(180deg)'
    },
    avatar: {
        backgroundColor: red[500]
    }
})

class RecipeReviewCard extends React.Component {
    handleExpandClick = () => {
        this.setState(state => ({ expanded: !state.expanded }))
    };

    render () {
        const {
            classes,
            topImg,
            title,
            img,
            desc
        } = this.props

        return (
            <Card className={ classes.card }>
                <CardTop
                    img={ topImg }
                    title={ title }
                />
                <CardItem
                    img={ img }
                />
                <CardDescription
                    desc={ desc }
                />
                <CardActions className={ classes.actions } disableActionSpacing>
                    <CardBottom />
                </CardActions>
            </Card>
        )
    }
}

RecipeReviewCard.propTypes = {
    classes : PropTypes.object.isRequired,
    topImg  : PropTypes.string.isRequired,
    title   : PropTypes.string.isRequired,
    img     : PropTypes.string.isRequired,
    desc    : PropTypes.string.isRequired
}

export default withStyles(styles)(RecipeReviewCard)
