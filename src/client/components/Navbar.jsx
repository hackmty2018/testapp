import React, { Component, Fragment } from 'react'
import EventListener from 'react-event-listener'
import PropTypes from 'prop-types'
import compose from 'recompose/compose'
import withWidth from '@material-ui/core/withWidth'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Hidden from '@material-ui/core/Hidden'
import Hot from '@material-ui/icons/Whatshot'
import Trending from '@material-ui/icons/TrendingUp'
import New from '@material-ui/icons/AccessTime'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import Button from '@material-ui/core/Button'

const drawerWidth = 240

const styles = theme => ({
    appBar: {
        position   : 'absolute',
        transition : theme.transitions.create(['margin', 'width'], {
            easing   : theme.transitions.easing.sharp,
            duration : theme.transitions.duration.leavingScreen
        })
    },
    appBarShift: {
        width       : `calc(100% - ${drawerWidth}px)`,
        marginLeft  : drawerWidth,
        transition : theme.transitions.create(['margin', 'width'], {
            easing   : theme.transitions.easing.easeOut,
            duration : theme.transitions.duration.enteringScreen
        })
    },
    menuButton: {
        marginLeft  : 12,
        marginRight : 20
    },
    hide: {
        display: 'none'
    },
    flex: {
        flexGrow: 1
    },
    iconButton: {
        margin: '0 16px'
    },
    button: {
        margin: theme.spacing.unit
    }
})

class Navbar extends Component {
    static propTypes = {
        handleOpenDrawer        : PropTypes.func.isRequired,
        classes                 : PropTypes.object.isRequired,
        width                   : PropTypes.string.isRequired,
        handleTabChange         : PropTypes.func.isRequired,
        handleUpdateIndicator   : PropTypes.func.isRequired,
        handleFirebaseLogin     : PropTypes.func.isRequired,
        user                    : PropTypes.object,
        tabIndex                : PropTypes.number,
        openDrawer              : PropTypes.bool,
        withTabs                : PropTypes.bool
    }

    static defaultProps = {
        withTabs    : false,
        tabIndex    : 0,
        openDrawer  : false,
        user        : []
    }

    state = {
        updateIndicator     : null,
        anchorEl            : null
    }

    _handleTabsActions = ({ updateIndicator }) => {
        const { handleUpdateIndicator } = this.props
        handleUpdateIndicator(updateIndicator)
        this.setState({ updateIndicator })
    }

    _handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget })
    }

    _handleClose = () => {
        this.setState({ anchorEl: null })
    }

    render () {
        const {
            openDrawer,
            handleOpenDrawer,
            classes,
            width,
            tabIndex,
            handleTabChange,
            withTabs,
            handleFirebaseLogin,
            user
        } = this.props
        const { updateIndicator, anchorEl } = this.state
        const open = Boolean(anchorEl)
        return (
            <Fragment>
                { updateIndicator && (
                    <EventListener
                        target="window"
                        onResize={ updateIndicator }
                    />
                )}
                <AppBar
                    className={ classNames(classes.appBar, {
                        [classes.appBarShift]: openDrawer && (width !== 'sm' && width !== 'xs')
                    }) }
                >
                    <Toolbar disableGutters={ !openDrawer }>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={ handleOpenDrawer }
                            className={ classNames(classes.menuButton, (openDrawer && width !== 'sm' && width !== 'xs') && classes.hide) }
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={ classes.flex }>
                            Kencho
                        </Typography>
                        { user !== null ? (
                            <Fragment>
                                <IconButton
                                    aria-owns={ open ? 'menu-appbar' : null }
                                    aria-haspopup="true"
                                    onClick={ this._handleMenu }
                                    color="inherit"
                                    className={ classes.iconButton }
                                >
                                    <AccountCircle />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={ anchorEl }
                                    anchorOrigin={{
                                        vertical   : 'top',
                                        horizontal : 'right'
                                    }}
                                    transformOrigin={{
                                        vertical   : 'top',
                                        horizontal : 'right'
                                    }}
                                    open={ open }
                                    onClose={ this._handleClose }
                                >
                                    <MenuItem onClick={ this._handleClose }>Profile</MenuItem>
                                    <MenuItem onClick={ this._handleClose }>My account</MenuItem>
                                </Menu>
                            </Fragment>
                        ) : (
                            <Button
                                className={ classes.button }
                                onClick={ handleFirebaseLogin }
                            >
                                Iniciar Sesion
                            </Button>
                        ) }
                    </Toolbar>
                    { withTabs && (
                        <Hidden
                            smDown
                        >
                            <Tabs
                                value={ tabIndex }
                                onChange={ handleTabChange }
                                action={ this._handleTabsActions }
                                fullWidth
                            >
                                <Tab icon={ <Hot /> } label="Popular" />
                                <Tab icon={ <Trending /> } label="Trending" />
                                <Tab icon={ <New /> } label="New" />
                            </Tabs>
                        </Hidden>
                    )}
                </AppBar>
            </Fragment>
        )
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    withWidth()
)(Navbar)
