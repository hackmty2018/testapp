import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
})

theme.overrides = {
    MuiAppBar: {
        colorPrimary: {
            backgroundColor: '#75A1FB'
        }
    },
    MuiCard: {
        root: {
            backgroundColor: '#FFFFF4'
        }
    }
}

export default theme
