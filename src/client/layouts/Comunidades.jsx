import React, { Component } from 'react'
import compose from 'recompose/compose'
import withWidth from '@material-ui/core/withWidth'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import styled from 'react-emotion'
// import AppBar from '../components/AppBar'
import NavBar from '../components/Navbar'
import Drawer from '../components/MuiDrawer'
// import Cards from '../components/Card'
import KenchoList from '../components/KenchoList'

const drawerWidth = 240

const MainContainer = styled.div`
    display: flex;
    height: 100%;
`

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appFrame: {
        height   : '100%',
        zIndex   : 1,
        overflow : 'auto',
        position : 'relative',
        display  : 'flex',
        width    : '100%'
    },
    drawerHeader: {
        display         : 'flex',
        alignItems      : 'center',
        justifyContent  : 'flex-end',
        padding         : '0 8px',
        ...theme.mixins.toolbar
    },
    content: {
        overflow                        : 'auto',
        display                         : 'flex',
        flexDirection                   : 'column',
        flexGrow                        : 1,
        position                        : 'relative',
        [theme.breakpoints.up('md')]    : {
            marginLeft: -drawerWidth
        },
        backgroundColor : theme.palette.background.default,
        padding         : theme.spacing.unit * 3,
        transition      : theme.transitions.create('margin', {
            easing    : theme.transitions.easing.sharp,
            duration  : theme.transitions.duration.leavingScreen
        })
    },
    contentShift: {
        marginLeft  : 0,
        transition  : theme.transitions.create('margin', {
            easing   : theme.transitions.easing.easeOut,
            duration : theme.transitions.duration.enteringScreen
        })
    },
    mainBackground: {
        backgroundColor: '#F0F5FF'
    }
})

class Comunidades extends Component {
    static propTypes = {
        classes                 : PropTypes.object.isRequired,
        width                   : PropTypes.string.isRequired,
        location                : PropTypes.object.isRequired,
        handleFirebaseLogin     : PropTypes.func.isRequired,
        user                    : PropTypes.object
    }

    static defaultProps = {
        user: null
    }

    state = {
        openDrawer      : false,
        tabIndex        : 0,
        updateIndicator : null
    }

    _handleUpdateIndicator = updateIndicator => {
        this.setState({ updateIndicator })
    }

    _handleOpenDrawer = () => {
        const { updateIndicator } = this.state
        if (updateIndicator) {
            updateIndicator()
        }
        this.setState({
            openDrawer: true
        })
    }

    _handleCloseDrawer = () => {
        const { updateIndicator } = this.state
        if (updateIndicator) {
            updateIndicator()
        }
        this.setState({
            openDrawer: false
        })
    }

    _handleTabChange = (event, value) => {
        this.setState({ tabIndex: value })
    }

    _handleChangeIndex = index => {
        this.setState({ tabIndex: index })
    }

    render () {
        const { openDrawer, tabIndex } = this.state
        const { classes, width, location, user, handleFirebaseLogin } = this.props
        return (
            <MainContainer>
                <div className={ classes.root }>
                    <div className={ classes.appFrame }>
                        <NavBar
                            openDrawer={ openDrawer }
                            handleOpenDrawer={ this._handleOpenDrawer }
                            tabIndex={ tabIndex }
                            handleTabChange={ this._handleTabChange }
                            handleUpdateIndicator={ this._handleUpdateIndicator }
                            handleFirebaseLogin={ handleFirebaseLogin }
                            user={ user }
                        />
                        <Drawer
                            open={ openDrawer }
                            closeDrawer={ this._handleCloseDrawer }
                            location={ location }
                        />
                        <main
                            className={ classNames(classes.content, {
                                [classes.contentShift]  : openDrawer,
                                [classes.contentTabs]   : width !== 'sm' && width !== 'xs'
                            }) }
                        >
                            <div className={ classes.drawerHeader } />
                            <div>
                                <KenchoList title="Kencho" descripcion="Esta es la mejor comunidad de todo Kencho Amiges, somes incluyentes aqui!" url="https://yt3.ggpht.com/a-/ACSszfHk1J6eNRUMzcjNXbnBB7BYfo2BwUo_BY6AYQ=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="MemesCx" descripcion="Lorem ipsum dolor sit amet consectetur adipiscing elit duis mattis, cras aptent blandit euismod ridiculus litora tristique vulputate" url="https://yt3.ggpht.com/a-/ACSszfE4S2gDvm1V_4uEYtIaMqzH3PrSlwktd77KhQ=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="Memes Luisito" descripcion="Lorem ipsum dolor sit amet consectetur adipiscing elit quisque aptent vehicula, proin taciti fames inceptos morbi torquent sed convallis curae." url="https://yt3.ggpht.com/a-/ACSszfHztk7BkaSJ05m_Z_3iGoHfUFadhk24i34vPA=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="Pewds Submisions" descripcion="Turpis bibendum ad nisl quisque fermentum torquent condimentum sociosqu volutpat vulputate convallis" url="https://yt3.ggpht.com/a-/ACSszfEGbyCokB_jO8kx2uokxt4vrFfjgK-YN8mGdA=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="Programming" descripcion="Eleifend feugiat imperdiet tristique hac sociis rhoncus, curae nascetur taciti ut fames cubilia, congue convallis primis mollis nostra." url="https://yt3.ggpht.com/a-/ACSszfGKJPnxMcdqbnTrcoGA4Nlc0Tf_Y2V80sSBCA=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="OriginalArt" descripcion="Dapibus volutpat pellentesque est sollicitudin tincidunt primis aliquam feugiat tortor, augue semper enim sagittis ut interdum viverra" url="https://yt3.ggpht.com/a-/ACSszfHZOHwXsoLgGVAL2v9kl8VxL-ZtQJbwZ4sIfA=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="Kencho" descripcion="Esta es la mejor comunidad de todo Kencho Amiges, somes incluyentes aqui!" url="https://yt3.ggpht.com/a-/ACSszfHk1J6eNRUMzcjNXbnBB7BYfo2BwUo_BY6AYQ=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="MemesCx" descripcion="Lorem ipsum dolor sit amet consectetur adipiscing elit duis mattis, cras aptent blandit euismod ridiculus litora tristique vulputate" url="https://yt3.ggpht.com/a-/ACSszfE4S2gDvm1V_4uEYtIaMqzH3PrSlwktd77KhQ=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="Memes Luisito" descripcion="Lorem ipsum dolor sit amet consectetur adipiscing elit quisque aptent vehicula, proin taciti fames inceptos morbi torquent sed convallis curae." url="https://yt3.ggpht.com/a-/ACSszfHztk7BkaSJ05m_Z_3iGoHfUFadhk24i34vPA=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="Pewds Submisions" descripcion="Turpis bibendum ad nisl quisque fermentum torquent condimentum sociosqu volutpat vulputate convallis" url="https://yt3.ggpht.com/a-/ACSszfEGbyCokB_jO8kx2uokxt4vrFfjgK-YN8mGdA=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="Programming" descripcion="Eleifend feugiat imperdiet tristique hac sociis rhoncus, curae nascetur taciti ut fames cubilia, congue convallis primis mollis nostra." url="https://yt3.ggpht.com/a-/ACSszfGKJPnxMcdqbnTrcoGA4Nlc0Tf_Y2V80sSBCA=s176-mo-c-c0xffffffff-rj-k-no" />
                                <KenchoList title="OriginalArt" descripcion="Dapibus volutpat pellentesque est sollicitudin tincidunt primis aliquam feugiat tortor, augue semper enim sagittis ut interdum viverra" url="https://yt3.ggpht.com/a-/ACSszfHZOHwXsoLgGVAL2v9kl8VxL-ZtQJbwZ4sIfA=s176-mo-c-c0xffffffff-rj-k-no" />
                            </div>
                        </main>
                    </div>
                </div>
            </MainContainer>
        )
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    withWidth()
)(Comunidades)

