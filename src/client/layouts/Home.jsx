import React, { Component } from 'react'
import compose from 'recompose/compose'
import withWidth from '@material-ui/core/withWidth'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import styled from 'react-emotion'
import Typography from '@material-ui/core/Typography'
import SwipeableViews from 'react-swipeable-views'
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction'
import Hot from '@material-ui/icons/Whatshot'
import Trending from '@material-ui/icons/TrendingUp'
import New from '@material-ui/icons/AccessTime'
import Hidden from '@material-ui/core/Hidden'
import Button from '@material-ui/core/Button'
// import AddIcon from '@material-ui/icons/Add'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
// import AppBar from '../components/AppBar'
import NavBar from '../components/Navbar'
import Drawer from '../components/MuiDrawer'
// import Cards from '../components/Card'
import Cards from '../components/Card'
import { storageRef } from '../../lib/startup/firebase'

import p1 from '../../assets/images/p-1.jpeg'

const drawerWidth = 240

const MainContainer = styled.div`
    display: flex;
    height: 100%;
`

const SwiperItem = styled.div`
    flex: 1 0 auto;
`

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appFrame: {
        height   : '100%',
        zIndex   : 1,
        overflow : 'auto',
        position : 'relative',
        display  : 'flex',
        width    : '100%'
    },
    drawerHeader: {
        display         : 'flex',
        alignItems      : 'center',
        justifyContent  : 'flex-end',
        padding         : '0 8px',
        ...theme.mixins.toolbar
    },
    content: {
        display                         : 'flex',
        flexDirection                   : 'column',
        flexGrow                        : 1,
        position                        : 'relative',
        overflow                        : 'auto',
        [theme.breakpoints.up('md')]    : {
            marginLeft: -drawerWidth
        },
        [theme.breakpoints.down('sm')]: {
            paddingBottom: 56
        },
        backgroundColor : theme.palette.background.default,
        padding         : theme.spacing.unit * 3,
        transition      : theme.transitions.create('margin', {
            easing    : theme.transitions.easing.sharp,
            duration  : theme.transitions.duration.leavingScreen
        })
    },
    contentShift: {
        marginLeft  : 0,
        transition  : theme.transitions.create('margin', {
            easing   : theme.transitions.easing.easeOut,
            duration : theme.transitions.duration.enteringScreen
        })
    },
    contentTabs: {
        paddingTop: 96
    },
    rootBottomNav: {
        position    : 'fixed',
        bottom      : 0,
        left        : 0,
        width       : '100%'
    },
    mainBackground: {
        backgroundColor: '#F0F5FF'
    },
    fab: {
        position                        : 'absolute',
        bottom                          : 32,
        right                           : 40,
        [theme.breakpoints.down('sm')]    : {
            bottom : 72,
            right  : 24
        }
    },
    button: {
        margin: theme.spacing.unit
    },
    input: {
        display: 'none'
    },
    prevImage: {
        width: 500
    }
})

class Home extends Component {
    static propTypes = {
        classes                 : PropTypes.object.isRequired,
        width                   : PropTypes.string.isRequired,
        location                : PropTypes.object.isRequired,
        handleFirebaseLogin     : PropTypes.func.isRequired,
        user                    : PropTypes.object
    }

    static defaultProps = {
        user: null
    }

    state = {
        openDrawer      : false,
        tabIndex        : 0,
        updateIndicator : null,
        openDialog      : false,
        file            : null,
        imagePreviewUrl : null
        // token           : null
    }

    _handleUpdateIndicator = updateIndicator => {
        this.setState({ updateIndicator })
    }

    _handleOpenDrawer = () => {
        const { updateIndicator } = this.state
        if (updateIndicator) {
            updateIndicator()
        }
        this.setState({
            openDrawer: true
        })
    }

    _handleCloseDrawer = () => {
        const { updateIndicator } = this.state
        if (updateIndicator) {
            updateIndicator()
        }
        this.setState({
            openDrawer: false
        })
    }

    _handleTabChange = (event, value) => {
        this.setState({ tabIndex: value })
    }

    _handleChangeIndex = index => {
        this.setState({ tabIndex: index })
    }

    _handleOpenDialog = () => {
        this.setState({ openDialog: true })
    }

    _handleCloseDialog = () => {
        this.setState({
            openDialog          : false,
            file                : null,
            imagePreviewUrl     : null
        })
    }

    _handleFileChange = e => {
        e.preventDefault()
        const reader = new FileReader()
        const file = e.target.files[0]

        reader.onloadend = () => {
            this.setState({
                file,
                imagePreviewUrl: reader.result
            })
        }

        reader.readAsDataURL(file)
    }

    _handleSubmitFile = () => {
        const { file } = this.state
        const imageRef = storageRef.child(file.name)
        imageRef.put(file).then(snapshot => {
            const { i } = snapshot.ref.getDownloadURL()
            console.log(i)
            this.setState({
                openDialog          : false,
                file                : null,
                imagePreviewUrl     : null
            })
        })
    }

    render () {
        const { openDrawer, tabIndex, openDialog, file, imagePreviewUrl } = this.state
        const { classes, width, location, user, handleFirebaseLogin } = this.props
        console.log(file)
        return (
            <MainContainer>
                <div className={ classes.root }>
                    <div className={ classes.appFrame }>
                        <NavBar
                            openDrawer={ openDrawer }
                            handleOpenDrawer={ this._handleOpenDrawer }
                            tabIndex={ tabIndex }
                            handleTabChange={ this._handleTabChange }
                            handleUpdateIndicator={ this._handleUpdateIndicator }
                            handleFirebaseLogin={ handleFirebaseLogin }
                            user={ user }
                            withTabs
                        />
                        <Drawer
                            open={ openDrawer }
                            closeDrawer={ this._handleCloseDrawer }
                            location={ location }
                        />
                        <main
                            className={ classNames(classes.content, {
                                [classes.contentShift]  : openDrawer,
                                [classes.contentTabs]   : width !== 'sm' && width !== 'xs'
                            }) }
                        >
                            <div className={ classes.drawerHeader } />
                            <SwipeableViews
                                index={ tabIndex }
                                onChangeIndex={ this._handleChangeIndex }
                                style={{
                                    display         : 'flex',
                                    flex            : '1 0 auto',
                                    flexDirection   : 'column'
                                }}
                            >
                                <SwiperItem>
                                    <Cards
                                        title="Post"
                                        topImg="https://yt3.ggpht.com/-rJq9gk1QIis/AAAAAAAAAAI/AAAAAAAAAAA/Kx4wkvKOfxY/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"
                                        img={ p1 }
                                        desc="Test"
                                    />
                                    <Cards
                                        title="Post"
                                        topImg="https://yt3.ggpht.com/-dPlY2nC8pL4/AAAAAAAAAAI/AAAAAAAAAAA/jB0ypPv2Wxo/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"
                                        img={ p1 }
                                        desc="Test"
                                    />
                                    <Cards
                                        title="Post"
                                        topImg="https://yt3.ggpht.com/-rJq9gk1QIis/AAAAAAAAAAI/AAAAAAAAAAA/Kx4wkvKOfxY/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"
                                        img={ p1 }
                                        desc="Test"
                                    />
                                    <Cards
                                        title="Post"
                                        topImg="https://yt3.ggpht.com/-dPlY2nC8pL4/AAAAAAAAAAI/AAAAAAAAAAA/jB0ypPv2Wxo/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"
                                        img={ p1 }
                                        desc="Test"
                                    />
                                    <Cards
                                        title="Post"
                                        topImg="https://yt3.ggpht.com/-rJq9gk1QIis/AAAAAAAAAAI/AAAAAAAAAAA/Kx4wkvKOfxY/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"
                                        img={ p1 }
                                        desc="Test"
                                    />
                                </SwiperItem>
                                <SwiperItem>
                                    <Typography>2</Typography>
                                </SwiperItem>
                                <SwiperItem>
                                    <Typography>3</Typography>
                                </SwiperItem>
                            </SwipeableViews>
                            <Hidden
                                mdUp
                            >
                                <BottomNavigation
                                    value={ tabIndex }
                                    onChange={ this._handleTabChange }
                                    className={ classes.rootBottomNav }
                                    showLabels
                                >
                                    <BottomNavigationAction label="Hot" icon={ <Hot /> } />
                                    <BottomNavigationAction label="Trending" icon={ <Trending /> } />
                                    <BottomNavigationAction label="New" icon={ <New /> } />
                                </BottomNavigation>
                            </Hidden>
                        </main>
                        {/* <Button
                            variant="fab"
                            color="primary"
                            aria-label="Add"
                            className={ classes.fab }
                            onClick={ this._handleOpenDialog }
                        >
                            <AddIcon />
                        </Button> */}
                    </div>
                </div>
                <Dialog
                    open={ openDialog }
                    onClose={ this._handleCloseDialog }
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Subir Imagen</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Sube una imagen a tu comunidad
                        </DialogContentText>
                        <div>
                            { imagePreviewUrl && <img alt="" src={ imagePreviewUrl } className={ classes.prevImage } /> }
                        </div>
                        {  /* eslint-disable */ }
                        <input
                            accept="image/*"
                            className={ classes.input }
                            id="contained-button-file"
                            multiple
                            type="file"
                            onChange={ this._handleFileChange }
                        />
                        <label htmlFor="contained-button-file">
                            <Button variant="contained" component="span" className={ classes.button }>
                                Subir
                            </Button>
                        </label>
                        { /* eslint-enable */ }
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={ this._handleCloseDialog } color="primary">
                            Cancelar
                        </Button>
                        <Button onClick={ this._handleSubmitFile } color="primary">
                            Subir
                        </Button>
                    </DialogActions>
                </Dialog>
            </MainContainer>
        )
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    withWidth()
)(Home)

