import React, { Component } from 'react'
import '../styles/layout.scss'
import 'typeface-roboto'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import theme from '../styles/mui/theme'
import Home from './Home'
import Comunidades from './Comunidades'
import Comunidad from './Comunidad'
import { app, googleProvider } from '../../lib/startup/firebase'

/* eslint-disable */
class App extends Component {
    state = {
        user: null
    }

    _handleFirebaseLogin = async () => {
        app.auth().signInWithPopup(googleProvider).then(result => {
            console.log(result)
            const { user } = result
            this.setState({ user })
            // this.setState({ user })
        }).catch(error => {
            console.log(error)
        })
    }

    render () {
        const { user } = this.state
        return (
            <MuiThemeProvider
                theme={ theme }
            >
                <Router>
                    <Switch>
                        <Route
                            path="/"
                            render={props => (
                                <Home
                                    { ...props }
                                    user={ user }
                                    handleFirebaseLogin={ this._handleFirebaseLogin }
                                />
                            )}
                            exact
                        />
                        <Route
                            path="/comunidades"
                            render={ props => (
                                <Comunidades
                                    { ...props }
                                    user={ user }
                                    handleFirebaseLogin={ this._handleFirebaseLogin }
                                />
                            )}
                            exact
                        />
                        <Route
                            path="/comunidades/poods"
                            render={ props => (
                                <Comunidad
                                    { ...props }
                                    user={ user }
                                    handleFirebaseLogin={ this._handleFirebaseLogin }
                                />
                            )}
                            exact
                        />
                        <Route
                            path="/comunidades/dolan"
                            render={ props => (
                                <Comunidad
                                    { ...props }
                                    user={ user }
                                    handleFirebaseLogin={ this._handleFirebaseLogin }
                                />
                            )}
                            exact
                        />
                    </Switch>
                </Router>
            </MuiThemeProvider>
        )
    }
}
/* eslint-enable */

export default App
