import React, { Component } from 'react'
import compose from 'recompose/compose'
import withWidth from '@material-ui/core/withWidth'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import styled from 'react-emotion'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { storageRef } from '../../lib/startup/firebase'
// import AppBar from '../components/AppBar'
import NavBar from '../components/Navbar'
import Drawer from '../components/MuiDrawer'
// import Cards from '../components/Card'
import GridImagenes from '../components/GridImagenes'
const drawerWidth = 240

const MainContainer = styled.div`
    display: flex;
    height: 100%;
`

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appFrame: {
        height   : '100%',
        zIndex   : 1,
        overflow : 'auto',
        position : 'relative',
        display  : 'flex',
        width    : '100%'
    },
    drawerHeader: {
        display         : 'flex',
        alignItems      : 'center',
        justifyContent  : 'flex-end',
        padding         : '0 8px',
        ...theme.mixins.toolbar
    },
    content: {
        overflow                        : 'auto',
        display                         : 'flex',
        flexDirection                   : 'column',
        flexGrow                        : 1,
        position                        : 'relative',
        [theme.breakpoints.up('md')]    : {
            marginLeft: -drawerWidth
        },
        backgroundColor : theme.palette.background.default,
        padding         : theme.spacing.unit * 3,
        transition      : theme.transitions.create('margin', {
            easing    : theme.transitions.easing.sharp,
            duration  : theme.transitions.duration.leavingScreen
        })
    },
    contentShift: {
        marginLeft  : 0,
        transition  : theme.transitions.create('margin', {
            easing   : theme.transitions.easing.easeOut,
            duration : theme.transitions.duration.enteringScreen
        })
    },
    mainBackground: {
        backgroundColor: '#F0F5FF'
    },
    fab: {
        position                        : 'absolute',
        bottom                          : 32,
        right                           : 40,
        [theme.breakpoints.down('sm')]    : {
            bottom : 72,
            right  : 24
        }
    },
    button: {
        margin: theme.spacing.unit
    },
    input: {
        display: 'none'
    },
    prevImage: {
        width: 500
    }
})

class Comunidades extends Component {
    static propTypes = {
        classes                 : PropTypes.object.isRequired,
        width                   : PropTypes.string.isRequired,
        location                : PropTypes.object.isRequired,
        handleFirebaseLogin     : PropTypes.func.isRequired,
        user                    : PropTypes.object
    }

    static defaultProps = {
        user: null
    }

    state = {
        openDrawer      : false,
        tabIndex        : 0,
        updateIndicator : null,
        openDialog      : false,
        file            : null,
        imagePreviewUrl : null
    }

    _handleUpdateIndicator = updateIndicator => {
        this.setState({ updateIndicator })
    }

    _handleOpenDrawer = () => {
        const { updateIndicator } = this.state
        if (updateIndicator) {
            updateIndicator()
        }
        this.setState({
            openDrawer: true
        })
    }

    _handleCloseDrawer = () => {
        const { updateIndicator } = this.state
        if (updateIndicator) {
            updateIndicator()
        }
        this.setState({
            openDrawer: false
        })
    }

    _handleTabChange = (event, value) => {
        this.setState({ tabIndex: value })
    }

    _handleChangeIndex = index => {
        this.setState({ tabIndex: index })
    }

    _handleOpenDialog = () => {
        this.setState({ openDialog: true })
    }

    _handleCloseDialog = () => {
        this.setState({
            openDialog          : false,
            file                : null,
            imagePreviewUrl     : null
        })
    }

    _handleFileChange = e => {
        e.preventDefault()
        const reader = new FileReader()
        const file = e.target.files[0]

        reader.onloadend = () => {
            this.setState({
                file,
                imagePreviewUrl: reader.result
            })
        }

        reader.readAsDataURL(file)
    }

    _handleSubmitFile = () => {
        const { file } = this.state
        const imageRef = storageRef.child(file.name)
        imageRef.put(file).then(snapshot => {
            console.log(snapshot)
            this.setState({
                openDialog          : false,
                file                : null,
                imagePreviewUrl     : null
            })
        })
    }

    render () {
        const { openDrawer, tabIndex, imagePreviewUrl, openDialog } = this.state
        const { classes, width, location, user, handleFirebaseLogin } = this.props
        console.log(location)
        return (
            <MainContainer>
                <div className={ classes.root }>
                    <div className={ classes.appFrame }>
                        <NavBar
                            openDrawer={ openDrawer }
                            handleOpenDrawer={ this._handleOpenDrawer }
                            tabIndex={ tabIndex }
                            handleTabChange={ this._handleTabChange }
                            handleUpdateIndicator={ this._handleUpdateIndicator }
                            handleFirebaseLogin={ handleFirebaseLogin }
                            user={ user }
                        />
                        <Drawer
                            open={ openDrawer }
                            closeDrawer={ this._handleCloseDrawer }
                            location={ location }
                        />
                        <main
                            className={ classNames(classes.content, {
                                [classes.contentShift]  : openDrawer,
                                [classes.contentTabs]   : width !== 'sm' && width !== 'xs'
                            }) }
                        >
                            <div className={ classes.drawerHeader } />
                            <div />
                            <GridImagenes />
                        </main>
                        <Button
                            variant="fab"
                            color="primary"
                            aria-label="Add"
                            className={ classes.fab }
                            onClick={ this._handleOpenDialog }
                        >
                            <AddIcon />
                        </Button>
                    </div>
                </div>
                <Dialog
                    open={ openDialog || false }
                    onClose={ this._handleCloseDialog }
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Subir Imagen</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Sube una imagen a tu comunidad
                        </DialogContentText>
                        <div>
                            { imagePreviewUrl && <img alt="" src={ imagePreviewUrl } className={ classes.prevImage } /> }
                        </div>
                        {  /* eslint-disable */ }
                        <input
                            accept="image/*"
                            className={ classes.input }
                            id="contained-button-file"
                            multiple
                            type="file"
                            onChange={ this._handleFileChange }
                        />
                        <label htmlFor="contained-button-file">
                            <Button variant="contained" component="span" className={ classes.button }>
                                Subir
                            </Button>
                        </label>
                        { /* eslint-enable */ }
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={ this._handleCloseDialog } color="primary">
                            Cancelar
                        </Button>
                        <Button onClick={ this._handleSubmitFile } color="primary">
                            Subir
                        </Button>
                    </DialogActions>
                </Dialog>
            </MainContainer>
        )
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    withWidth()
)(Comunidades)

